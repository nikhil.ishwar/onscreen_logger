import 'dart:math';

import 'package:flutter/material.dart';

class TestFull extends StatefulWidget {
  TestFull({Key? key}) : super(key: key) {
    print("Test full constructor");
  }

  @override
  State<TestFull> createState() {
    print("Create state called");
    return _TestFullState();
  }
}

class _TestFullState extends State<TestFull> {
  @override
  Widget build(BuildContext context) {
    var list = [Colors.red, Colors.black, Colors.blue, Colors.purpleAccent];

// generates a new Random object
    final _random = new Random();

// generate a random index based on the list length
// and use it to retrieve the element
    var element = list[_random.nextInt(list.length)];
    print("Build called");
    return Container(
      color: element,
      width: 100,
      height: 100,
    );
  }

  @override
  void initState() {
    print("Init state called");
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      print("Binding observer called");
    });
  }
}
